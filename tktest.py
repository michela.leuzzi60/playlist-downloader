import tkinter as tk 
from tkinter import * 
from tkinter import filedialog
import traceback
import playlistdownloader as pd
from playlistdownloader import download_playlist

WIDTH = 300
HEIGHT = 200

class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()
        try:
            self.input_widget()
        except:
            print("There has been an error")

    def input_widget(self):
        label = tk.Label(self, text="Enter the playlist link:", justify=LEFT)
        label.grid(row=1)

        self.plinput = tk.Entry(self)
        self.plinput["justify"] = CENTER
        self.plinput["bd"] = 5
        self.plinput.grid(row=2)

        labelpath = tk.Label(self, text="Select the path:", justify=LEFT)
        labelpath.grid(row=3)

        self.pathinput = tk.Entry(self)
        self.pathinput["bd"] = 5
        self.pathinput.grid(row=4)

        button = tk.Button(self, text="Enter")
        button["command"] = self.execute_download
        button.grid(row=5)




    def execute_download(self):
        l = self.plinput.get()
        p = self.pathinput.get()

        # totaltracks = tk.Label(self, text="hello there")
        # totaltracks.grid(row=8)

        try:
            download_playlist(link=l, path=p)
            #totaltracks.configure("downloading " + str(pd.total) + " tracks")
        except:
            traceback.print_exc()
            print("there has been an error downloading the playlist")

        # totaltracks.configure("download complete!")
        # totaltracks.sleep(3)
        # totaltracks.configure("")
        self.plinput.delete(0, len(l)+1) #clears field when it's entered
        self.pathinput.delete(0, len(p)+1)


root = tk.Tk()
root.geometry(str(WIDTH) + "x" + str(HEIGHT))
app = Application(master=root)
app.mainloop()