import tkinter as tk 
from tkinter import * 

class Application(tk.Frame):
    #master = parent widget - passed to the new instance of Application when it's initialized
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master 
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        self.hi_there = tk.Button(self)
        self.hi_there["text"] = "Hello world\n(click me)"
        self.hi_there["command"] = self.say_hi
        self.hi_there.pack(side = "top")

        self.quit = tk.Button(self, text="QUIT", fg="red", command=self.master.destroy)
        self.quit.pack(side="bottom")

    def say_hi(self):
        print("hi everyone")

root = tk.Tk()
app = Application(master=root)
app.mainloop()