#simple program to download individual videos 
import pytube as pt
from pytube import YouTube

link = input("link: ")
yt = YouTube(link)

#Title of video
print("title: ", yt.title)
#Author
print("author: ", yt.author)
#Number of views of video
print("Number of views: ",yt.views)
#Length of the video
print("Length of video: ",format(yt.length/60, '.2f'), "minutes")
#Rating
print("Ratings: ",format(yt.rating, '.2f'))
#Streams -- all the video qualities
#print(yt.streams)
#streams -- filtering - progessive streams have both audio and video codecs
#print(yt.streams.filter(progressive=True))
#highest resolution stream -- can also get_by_itag('tag')
hq = yt.streams.get_highest_resolution()
# get size - convert to gb -- use format to limit to 2 decimal places
print("size in gb (of highest resolution stream): ", format(hq.filesize_approx*(9.31*10**(-10)), '.2f'))
#downloading -- the r converts from normal string tohttps://www.youtube.com/watch?v=JMMiLv3qJj0 raw string
print("downloading......")
hq.download(r'C:\Users\miche\OneDrive\Desktop\ytdw')
print("download complete!")
