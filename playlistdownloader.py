import pytube as pt 
from pytube import YouTube, Playlist
import subprocess
import os
import re
import time
import traceback

downloaded = 0;
total = 0;

def download_playlist(link, path):
    playlist = Playlist(link)
    total = len(playlist.video_urls)
    print(total)

    for url in playlist.video_urls:
        try:
            yt = YouTube(url)
            name = r"" + (yt.title)
            #These characters throw errors -- try to find out why 
            name = name.replace("'", "").replace("?", "").replace(",", "")
            print(name)
            yt.streams.filter(only_audio = True, file_extension='mp4').first().download(path)
            file = path + "\\" + name + '.mp4'
            mp3 = path + "\\" + name + '.mp3'
            os.rename(file, mp3)
            global downloaded 
            downloaded += 1

        except:
            file = path + "\\" + name + '.mp4'
            os.remove(file)
            print("already downloaded")

    print("\ndone\n")



